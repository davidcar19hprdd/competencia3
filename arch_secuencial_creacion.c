#include <stdio.h>
#include <time.h>

#define TAM_NOMBRE 100
#define TAM_NUMEROS 1000000

int existe_archivo(char *nombre_arch);
int crear_archivo(char *nombre_arch);
void cerrar_archivo(FILE *ptrArchivo, char *nombre_arch);
FILE *abrir_Arcchivo_lectura_escritura(char *nombre_arch);
int tiene_datos_arch(char *nombre_arch);
FILE *abrir_Archivo_solo_Lectura(char *nombre_arch);
void imprimir_vector(int vector[], int tam);
void ordBurbuja(int a[], int n); 
int total_numeros_Arch(char *nombre_arch);


int main( int argc, char const *argv[]){
	clock_t start, end;
	double mili_segundos;
	double minutos;
	double segundos;
	 
	// File es una estructura
	FILE *ptrCf = NULL;
	int existe_arch = 0, total_numeros =0, numero_introducir =0;
	char nom_archivo[TAM_NOMBRE];
	int contador_numeros = 0;
	int numero_del_arch= 0;
	int vector[TAM_NUMEROS], i = 0;
 	int numeros_desde_archivo =0;
	 
	do{
		printf("\tIntroduzca el Nombre del Archivo a Crear:  ");
		fgets(nom_archivo);
		existe_arch = existe_archivo(nom_archivo);
		if(existe_arch){
			printf("*********************************************\n");
			printf("\tEl Archivo %s YA existe.\n", nom_archivo);
			printf("*********************************************\n");

			} else {
	
			printf("*********************************************\n");
			printf("\tEl Archivo %s NO existe.\n", nom_archivo);
			printf("*********************************************\n");
			}
			    if(crear_archivo(nom_archivo){
			printf("*********************************************\n");
			printf("\tEl Archivo %s se ha creado Correctamente.\n", nom_archivo);
			printf("*********************************************\n");
			
			}else{
			printf("*********************************************\n");
			printf("\tFalla en la creacion del archivo %s .\n", nom_archivo);
			printf("*********************************************\n");
	}
}
	}while(existe_arch);

	do{
		printf("\tIngrese el total de numero a escribir sobre el archivo: --> %s ", nom_archivo);
		scanf("%d", &total_numeros);
		if(total_numeros < 1 || total_numeros > TAM_NUMEROS)
			printf("\t\t------>El total debera ser mayor a 0 y menor que %d\n",TAM_NUMEROS); 
	}while(total_numeros < 1 ||total_numeros > TAM_NUMEROS);
	ptrCf = abrir_Archivo_lectura_escritura(nom_archivo);
	if(ptrCf ==NULL){
			printf("*********************************************\n");
			printf("\tEl Archivo %s NO se pudo abrir.\n", nom_archivo);
			printf("*********************************************\n");


			} else {
			printf("*********************************************\n");
			printf("\tEl Archivo %s se abrio de forma correcta.\n", nom_archivo);
			printf("********************************************************\n");
			printf("-------------------------ESCRITURA---------------------\n");
	printf("__> (Unix - ^d para terminar, Windows - ^z para terminar) Introduzca un numero [%d]",i);
			scanf("%d", &numero_introducir);
			while(!feof(stdin)){
				fprintf(ptrCf, "%d\n", numero_introducir);
				i++;
				if((i == total_numeros){
					break;
				}
	printf("--->Unix - ^d para terminar, Windows - ^z para terminar) Introduzca un numero [%d]",i);
	scanf("%d", &numero_introducir);
                    }
		printf("\n");		
		printf("++++++++++++++++++++++++++++++++++++++\n");
		printf("\t Escritura Terminada.\n");
		printf("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-\n");
		cerrar_archivo(ptrCf, nom_archivo);
}
if(tiene_datos_arch(nom_archivo)){
	
		printf("¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬\n");
		printf("\t El archivo %s contiene datos .\n",nom_archivo);
		printf("¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬\n");
		printf("______________LECTUra_________________________\n");
		ptrCf= abrir_Archivo_solo_Lectura(nom_archivo);
		if(ptrCf == NULL){
			printf("*********************************************\n");
			printf("\tEl Archivo %s No se pudo abrir.\n", nom_archivo);
			printf("********************************************************\n");
			}
			else{
			printf("*********************************************\n");
			printf("\tEl Archivo %s esta abierto correctamente.\n", nom_archivo);
			printf("********************************************************\n");
			printf("\t%s\n\n", "Numeros");
			// equivalente a funcion scanf 
			fscanf(ptrCf, "%d", &numero_del_arch);
			while(!feof(ptrCf)){
				printf("\t -> [%d] = %d\n", contador_numeros, numero_del_arch);
				vector[contador_numeros] = numero_del_arch;
				contador_numeros++;
				fscanf(ptrCf, "%d", &numero_del_arch);
				}
				printf("\tTotal de numeros leidos = %d\n\n", contador_numeros);
				imprimir_vector(vector, contador_numeros);
				printf("-------------------------\n");
				printf("LECTURA TERMINADA!!!\n");
				printf("-------------------------");
				cerrar_archivo(ptrCf, nom_archivo);
			}
			numeros_desde_archivo = total_numeros_Arch(nom_archivo);
			printf("-------------------------------------\n");
			printf("ORDENACION POR BURBUJA.\n");
			printf("-------------------------------------\n");
			printf("Lista original de %d elementos:\n\n", numeros_desde_archivo);
			imprimir_vector(vector, numeros_desde_archivo);
			start = clock();
			ordBurbuja(vector, numeros_desde_archivo);
			end = clock();
			printf("\n\t----->Ascendente<--------"); 
			printf("Lista ordenada de %d elementos:\n\n", numeros_desde_archivo);
			imprimir_vector(vector, numeros_desde_archivo);
			segundos = ((double) (end - start)) / (CLOCKS_PER_SEC);
			mili_segundos = (segundos/ 0.001);
			minutos = (segundos * 1) / 60;
			printf("\n\tTiempo en segundos=%lf s.\n", segundos);
			printf("\tTiempo en mili_segundos=%lf ms.\n", mili_segundos);
			printf("\tTiempo en minutos=%lf min.\n", minutos);
			printf("\n\tTotal de numeros leidos = %d.\n", numeros_desde_archivo);
			}
			else{
				printf("¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬\n");
				printf("\tEl archivo>> %s no tiene datos.\n", nom_archivo);
				printf("¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬¬\n");
			}
			printf("\n");
			return 0;

}
void ordBurbuja(int a[], int n){
	int interruptor = 1;
	int pasada, j, aux;
	for(pasada=0; pasada < n-1 && interruptor; pasada++){
			interruptor = 0;
 			for(j = 0; j < n-pasada-1; j++)
				if(a[j] > a[j+1]){
					interruptor = 1;
					aux = a[j];
					a[j] = a [j+1];
					a[j+1] = aux;
					}
				}
		}

void imprimir_vector(int vector[], int tam){
	int i;
	for(i = 0; i < tam; i++){
		printf("%5d", vector[i]);
		if((i+1)%5 == 0)
		 printf("\n");
		}
		printf("\n");

}

int total_numeros_Arch(char *nombre_arch){
	int lee_linea = 0, contador = 0;
	FILE *ptrArchivo;
	ptrArchivo = abrir_Archivo_solo_Lectura(nombre_arch);
	if(ptrArchivo == NULL){
printf("El archivo  %s  NO se pudo abrir desde total_numeros().\n", nombre_arch);	//////    \n",nombre_arch);				
	}
	else{
		printf("Archivo -> %s <- abierto desde total_numeros().\n", nombre_arch);
		// leo si hay datos
		fscanf(ptrArchivo, "%d", &lee_linea);
		// mientras no sea fin de archivo.
		while(!feof(ptrArchivo)){
			contador++;
			// leo si hay datos
			fscanf(ptrArchivo, "%d", &lee_linea);			
		}
		cerrar_archivo(ptrArchivo, nombre_arch);
	}
	return contador;
}
​
int tiene_datos_arch(char *nombre_arch){
	int hay_Datos = 0;
	FILE *ptrArchivo;
	int lee_linea = 0, contador = 0;
	ptrArchivo = abrir_Archivo_solo_Lectura(nombre_arch);
	if(ptrArchivo == NULL){
		printf("El archivo -> %s <- NO se pudo abrir desde tiene_datos_arch().\n", nombre_arch);			
		hay_Datos = 0;
	}
	else{
		printf("Archivo -> %s <- abierto desde tiene_datos_arch().\n", nombre_arch);
		// leo si hay datos
		fscanf(ptrArchivo, "%d", &lee_linea);
		// mientras no sea fin de archivo.
		while(!feof(ptrArchivo)){
			contador++;
			// leo si hay datos
			fscanf(ptrArchivo, "%d", &lee_linea);
			if(contador > 0){
				hay_Datos = 1;
				break;
			}
		}
		cerrar_archivo(ptrArchivo, nombre_arch);
	}	
	return hay_Datos;
}
​
FILE *abrir_Archivo_solo_Lectura(char *nombre_arch){
	FILE *ptrArchivo;
	ptrArchivo = fopen(nombre_arch, "r");
	if( ptrArchivo == NULL ){
		// con r Abre un archivo para lectura.
		printf("---> El archivo -> %s <- NO pudo abrirse (uso de r).\n", nombre_arch);
	}	
	else{
		printf("---> Archivo -> %s <- Abierto (uso de r).\n", nombre_arch);
	}
	return ptrArchivo;
}
​
FILE *abrir_Archivo_lectura_escritura(char *nombre_arch){
	FILE *ptrArchivo;
	ptrArchivo = fopen(nombre_arch, "r+");
	if( ptrArchivo == NULL ){
		// con r+ Abre un archivo para actualización (lectura y escritura).
		printf("---> El archivo -> %s <- NO pudo abrirse (uso de r+).\n", nombre_arch);
	}	
	else{
		printf("---> Archivo -> %s <- Abierto (uso de r+).\n", nombre_arch);
	}
	return ptrArchivo;
}
​
int existe_archivo(char *nombre_arch){
	FILE *ptrArchivo;
	int existe = 0;
	// Con r abre un archivo para lectura.
	ptrArchivo = fopen(nombre_arch, "r");
	if( ptrArchivo != NULL ){
		existe = 1;
		cerrar_archivo(ptrArchivo, nombre_arch);
	}	
	return existe;
}
​
int crear_archivo(char *nombre_arch){
	int creado = 0;
	FILE *ptrArchivo;
	ptrArchivo = fopen(nombre_arch, "w");
	if( ptrArchivo == NULL ){
		// con w crea un archivo para escritura. Si el archivo ya existe, descarta el contenido actual.
		printf("----> El archivo -> %s <- no pudo crearse (abrir; uso de w).\n", nombre_arch);
	}	
	else{
		creado = 1;
		printf("----> Archivo -> %s <- Creado (abierto; uso de w).\n", nombre_arch);
		cerrar_archivo(ptrArchivo, nombre_arch);
	}
	return creado;
}
​
void cerrar_archivo(FILE *ptrArchivo, char *nombre_arch){
	fclose(ptrArchivo);
	printf("----> Archivo -> %s <- Cerrado Correctamente.\n", nombre_arch);
}

























